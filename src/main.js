import Vue from "vue";
import {
  ColorPicker,
  Tooltip,
  Button,
  Input,
  Upload,
  Message,
  MessageBox
} from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import lang from "element-ui/lib/locale/lang/es";
import locale from "element-ui/lib/locale";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import progressBar from "progressbar.js";
import vuedraggable from "vuedraggable";
import "./static/index.css";
import "./assets/css/main.css";
import "vue-material-design-icons/styles.css";
import CloseBox from "vue-material-design-icons/CloseBox.vue";
import CheckBold from "vue-material-design-icons/CheckBold.vue";
import ArrowRight from "vue-material-design-icons/ArrowRight.vue";
import ArrowLeft from "vue-material-design-icons/ArrowLeft.vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus, faWindowClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

Vue.prototype.$message = Message;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;

Vue.component("font-awesome-icon", FontAwesomeIcon);
library.add(faPlus, faWindowClose);
Vue.component("close-box-icon", CloseBox);
Vue.component("check-bold-icon", CheckBold);
Vue.component("arrow-right-icon", ArrowRight);
Vue.component("arrow-left-icon", ArrowLeft);
locale.use(lang);
Vue.use(ColorPicker);
Vue.use(Tooltip);
Vue.use(Input);
Vue.use(Button);
Vue.use(Upload);
Vue.component(Message.name, Message);
Vue.component(MessageBox.name, MessageBox);
// Vue.component(CollapseTransition.name, CollapseTransition)
Vue.use(vuedraggable);
Vue.use(progressBar);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
