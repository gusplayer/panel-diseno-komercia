import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
// import getCookie from "./utils/getCookie";

Vue.use(Vuex);
// let token =
//   "";

export default new Vuex.Store({
  state: {
    indexDevice: 3,
    idView: null,
    views: [],
    token:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJjYjllYjk4OWZiOGYzZTA5NzZhYjViMGJiNzhmNzI3ZGM0ODc3YTYwMjRlMWE3NzYyMTc2ZjE3OWNmYTc3NTFlYmYzNWViMGVhMzk1MTJiIn0.eyJhdWQiOiIyIiwianRpIjoiMmNiOWViOTg5ZmI4ZjNlMDk3NmFiNWIwYmI3OGY3MjdkYzQ4NzdhNjAyNGUxYTc3NjIxNzZmMTc5Y2ZhNzc1MWViZjM1ZWIwZWEzOTUxMmIiLCJpYXQiOjE1NzAxNDU2MjUsIm5iZiI6MTU3MDE0NTYyNSwiZXhwIjoxNTcyNzM3NjI1LCJzdWIiOiIxMDQ5Iiwic2NvcGVzIjpbXX0.d35aWULnJcm2MJhlgKgiJk8I6NF_zTi0R6AkJvQaruqi6FnmquAb5PTnSIGwD_IYCe-vPnxjDtzPe2-2tspDV0bMATn1iJmGQUWkoeM_CCysa57aXsvTK1Z-DEfhBQnhYhXSZ2GAdz2m541uZ0jGNgj_tFpoviyNcxMDmFVHn1NzDkyICA89XJbJQmIzCEfc0K0trR_29Yda32hAGe0oTAzepBwQczK1fZr4GsBh4xoIo226ze7t-68ClHPBTDTGzjWWiSO9GuyFCL9vg3hEid_ZY0gpKJVJSBsbzVmcXG1GCNKrcsBPub_wtrfnjo3CBzGPSUkHDOx-2QnvxWw7_CCO7s8LqiLevUtCTfMeFDBgk5Nrg4sBQAga3pqn7kNZNCpBmExEB6xk7pFm6hlfKnBQw477PJJvq5Am9pNlOc1AkPWijQelsGgsxBA0gJUNOOJYtTTvweU0rDkFw-NGpLFVx0bCEHf4p-zUFCsKjHzPWyP1QHMBKFRFpcALAkvjja66qopDjZV-dl5q3PuElNAY60pVWFQsGDfa6Bqg-Ird9aK8XgeqjKTItuMzQw2sAF6LmEnrelGGixsGdcOGMEjm1IF_FBPHlVphNNEuyNbbQBgYZI5njLvcugFB47hvGgYZ5R0NYhlX5bsITCbWgefIipq91M7LhLNAICK05hM",
    // indexType: 1,
    componentSelect: null,
    errorView: {
      toggle: false,
      message: ""
    },
    listComponents: [],
    toggleSettings: false,
    typeComponents: [],
    componentsByReference: [],
    urlComponents: "http://components.komercia.co",
    urlKomercia: "https://api2.komercia.co",
    configAxios: {
      headers: {
        "content-type": "application/json",
        Authorization: "",
        "Access-Control-Allow-Origin": "*",
        Access: "application/json"
      }
    },
    newView: "",
    accessToken: "",
    activatedButtonSave: false,
    putDragHeader: false,
    putDragBody: true,
    previewHeaderData: []
  },
  mutations: {
    SET_INDEXDEVICE(state, value) {
      state.indexDevice = value;
    },
    SET_PREVIEW_HEADER_DATA(state, value) {
      state.previewHeaderData = value;
    },
    SET_INDEXVIEW(state, value) {
      state.idView = value;
    },
    SET_PUT_DRAG_HEADER(state, value) {
      state.putDragHeader = value;
    },
    SET_PUT_DRAG_BODY(state, value) {
      state.putDragBody = value;
    },
    SET_COMPONENT_SELECT(state, value) {
      state.componentSelect = value;
    },
    SET_ACCESSTOKEN(state, value) {
      state.accessToken = value;
    },
    SET_toggleSettings(state, value) {
      state.toggleSettings = value;
    },
    SET_TYPES_COMPONENTS(state, value) {
      state.typeComponents = value;
    },
    SET_COMPONENTS_BY_REFERENCE(state, value) {
      state.componentsByReference = value;
    },
    SET_LIST_COMPONENTS(state, value) {
      state.listComponents = value;
    },
    SET_VIEWS(state, value) {
      state.views = value;
    },
    SET_NEW_VIEW(state, value) {
      state.newView = value;
    },
    SET_ERROR_VIEW(state, value) {
      state.errorView = {
        toggle: !state.errorViewtoggle,
        message: value
      };
    },
    SET_BUTTON_SAVE(state, value) {
      state.activatedButtonSave = value;
    }
  },
  actions: {
    GET_LOGIN({ state, commit, dispatch }) {
      // const token = getCookie("authData");
      axios
        .post(
          `${state.urlComponents}/api/login`,
          { token: state.token },
          state.configAxios
        )
        .then(async response => {
          commit("SET_ACCESSTOKEN", await response.data.access_token);
          dispatch("GET_TYPES_COMPONENTS");
          dispatch("GET_COMPONENTS_BY_REFERENCE");
          dispatch("GET_VIEWS");
        });
    },
    async GET_TYPES_COMPONENTS({ state, commit }) {
      if (state.accessToken) {
        let result = await axios.get(
          `${state.urlComponents}/api/components/types`,
          {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          }
        );
        commit("SET_TYPES_COMPONENTS", result.data.data);
      }
    },
    GET_COMPONENTS_BY_REFERENCE({ state, commit }, id = 1) {
      axios
        .get(`${state.urlComponents}/api/components/references/${id}`, {
          headers: {
            Authorization: `Bearer ${state.accessToken}`
          }
        })
        .then(response => {
          commit("SET_COMPONENTS_BY_REFERENCE", response.data.data);
          state.toggleSettings = false;
        });
    },
    GET_VIEWS({ state, commit }) {
      axios
        .get(`${state.urlComponents}/api/views`, {
          headers: {
            Authorization: `Bearer ${state.accessToken}`
          }
        })
        .then(response => {
          commit("SET_VIEWS", response.data.data);
        });
    },
    GET_COMPONENTS_BY_VIEW({ state, commit }, id = 1) {
      axios
        .get(`${state.urlComponents}/api/components/${id}`, {
          headers: {
            Authorization: `Bearer ${state.accessToken}`
          }
        })
        .then(response => {
          commit("SET_LIST_COMPONENTS", response.data.data);
        });
    },
    CREATE_VIEW({ state, dispatch, commit }, params) {
      axios
        .post(`${state.urlComponents}/api/views`, params, {
          headers: {
            Authorization: `Bearer ${state.accessToken}`
          }
        })
        .then(response => {
          commit("SET_NEW_VIEW", response.data.data);
          dispatch("GET_VIEWS");
        })
        .catch(error => {
          for (const key in error.response.data.errors) {
            if (error.response.data.errors.hasOwnProperty(key)) {
              const element = error.response.data.errors[key];
              commit("SET_ERROR_VIEW", element[0]);
            }
          }
        });
    },
    REMOVE_VIEW({ state, dispatch }, id) {
      axios
        .delete(`${state.urlComponents}/api/views/${id}`, {
          headers: {
            Authorization: `Bearer ${state.accessToken}`
          }
        })
        .then(() => {
          dispatch("GET_VIEWS");
        })
        .catch(error => {
          console.error(error.response);
        });
    },
    SEND_COMPONENTS({ state }) {
      let arrayId = [];
      state.listComponents.forEach(element => {
        if (element.reference) {
          arrayId.push(element.reference.id);
        } else {
          arrayId.push(element.id);
        }
      });
      axios
        .post(
          `${state.urlComponents}/api/views/components/add`,
          {
            view: state.idView,
            components: arrayId
          },
          {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          }
        )
        .then(response => {
          state.activatedButtonSave = false;
        });
    },
    GET_PREVIEW_HEADER_DATA({ state }, value) {
      if (value.length > 1) {
        state.previewHeaderData.splice(0, 1, value.shift());
      } else {
        state.previewHeaderData = value;
      }
    }
  }
});
